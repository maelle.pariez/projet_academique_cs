#!/bin/bash

# Mettre à jour les paquets existants
sudo apt-get update

# Installer AWS CLI
sudo apt install awscli

# Installer Git
sudo apt-get install git -y

# Installer Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Ajouter l'utilisateur courant au groupe Docker pour éviter de devoir utiliser sudo à chaque fois
sudo usermod -aG docker $USER

# S'il s'agit d'une VM EC2 ubuntu
sudo usermod -a -G docker ubuntu
id ubuntu
newgrp docker

# Activer le service docker
sudo systemctl enable docker.service
sudo systemctl start docker.service

# Installer Kubernetes
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# Installer Kind
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/local/bin

# Installer Terraform
sudo apt-get install -y unzip
wget https://releases.hashicorp.com/terraform/1.4.4/terraform_1.4.4_linux_amd64.zip
unzip terraform_1.4.4_linux_amd64.zip
sudo mv terraform /usr/local/bin/

# Afficher la version de chaque outil installé pour vérifier que tout s'est bien passé
echo "Version de Git :"
git --version

echo "Version de Docker :"
docker --version

echo "Version de Kubernetes :"
kubectl version --client

echo "Version de Kind :"
kind --version

echo "Version de Terraform :"
terraform version
