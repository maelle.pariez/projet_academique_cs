# PROJET FIL ROUGE - MS SIO

## PRE-REQUIS
- L'utilisation du programme (précisément le module TLA) nécessite d'avoir une key pour l'utilisation de l'API openAI. Si vous n'en avez pas, vous rendre sur [openAI](https://platform.openai.com/account/api-keys) pour vous créer une clé en cliquant sur le bouton "create new secret key".

- Si vous souhaitez déployer le programme sur Amazon Web Services pour profiter de ces services, il vous faudra un compte AWS.

- Pour finir, l'utilisation du programme nécessite d'avoir installé python dans une version antérieure ou égale à la version 3.10 pour des raisons de compatibilité avec les bibliothèques utilisées.

## FONCTIONNEMENT DU PROGRAMME
Le programme se décompose en plusieurs parties :

- [ProjetPythonPDF](https://gitlab-student.centralesupelec.fr/maelle.pariez/projetpythonpdf/) : API Flask permettant à un utilisateur de récupérer les métadonnées d'un PDF en uploadant un fichier de type PDF.

- Le programme fil rouge qui fonctionne en exécutant les modules suivants dans cet ordre :
    - [Module extraction](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/extraction)
    - [Module tla](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/tla)
    - [Module sémantisation](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/semantisation)
    - [Module exploitation](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/exploitation)

- Dans le repertoire correspondant à chacun de ces modules, se trouvent :
    - Un fichier README explicatif du module
    - Un fichier requirements.txt correspondant aux dépendances nécessaires au bon fonctionnement du programme
    - Un script .py portant le nom du module
    - Un fichier Dockerfile permettant de créer une image du module et de l'exécuter dans un docker

- Dans le répertoire des modules extraction et TLA, se trouve en plus :
    - Des fichiers .yaml permettant de créer des pods correspondant aux images de chaque module pour déployer l'application avec Kubernetes.


## INSTALLATION

### Pré-requis :
- Git, AWSCLI, Docker, Kubernetes, Kind et Terraform peuvent être utiles pour la suite.

#### Installation sur une nouvelle machine virtuelle vide :
Le fichier [install_tools.sh](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/blob/main/install_tools.sh) vous sera utile si vous souhaitez installer le projet depuis une machine virtuelle ubuntu vierge. Vous pourrez ainsi installer l'ensemble des pré-requis cité plus haut.

Configuration recommandée pour accueillir le projet : VM EC2 de type m5.large avec un volume de 50 Go

### Installation du projet 

    mkdir Projet_PFR_Maelle_PARIEZ
    cd Projet_PFR_Maelle_PARIEZ
    git clone https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs.git
    cd projet_academique_cs


## UTILISATION VIA DES NOTEBOOKS JUPYTER

L'ensemble du projet est exécutable en local depuis le répertoire [Notebook](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/notebook) sous réserve d'installer les dépendances nécessaires et d'avoir une version de python compatible : 3.10.

Vous pouvez effectuer installer l'ensemble des [librairies](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/blob/main/requirements.txt) à l'aide de la commande :
    
    pip install requirements.txt 


Pour utiliser le projet, vous devez exécuter les scripts dans l'ordre de numérotation de 1 à 4.
Cela vous génerera, tour après tour, les répertoires contenant les fichiers JSON correspondant.

**DATASET**

Vous disposez également de données de test dans le réprtoire [Fichiers_obtenus_pour_tests](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/notebook/Fichiers_obtenus_pour_tests) si vous souhaitez tester le programme avec des fichiers générés par celui-ci durant la phase de développement.

**API** 

Le répertoire [Notebook_API](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/notebook/Notebook_API) correspond au module exploitation traduit en API afin de permettre aux utilisateurs d'effectuer des requêtes finales et d'exporter des graphes de connaissance.


## UTILISATION EN LOCAL

### Pré-requis
- Avoir créé les buckets S3 suivants sur AWS :
    - bucket-extraction
    - bucket-tla
    - bucket-semantisation
Si vous ne le faîtes pas, vous rencontrerez une erreur au moment du peuplement des buckets et lors de la récupération des objets situés sur AWS (buckets).
- Configurer vos credentials AWS et OpenAI 

### Utilisation avec Docker

Il faut lancer un docker par module dans l'ordre : extraction, tla, semantisation, exploitation.

Etapes à répéter pour chacun des modules en changeant les ports pour le lancement de chaque nouveau docker :

1. Vous placer dans le répertoire extraction

2. Créer le docker

        (sudo) docker build -t extraction .

3. Lancer le docker

        (sudo) docker run -d -p 5000:5000 --name extraction extraction

Répeter l'opération pour les 3 autres modules.


### Utilisation sans Docker

Vous rendre dans chaque répertoire, installer les librairies à l'aide de la fonction :
    
    pip install requirements.txt

Puis exécuter le code python depuis le répertoire courant (pas à l'intérieur des repertoires de chaque module) à l'aide des commandes suivantes dans l'ordre :

    python extraction/extraction.py 
    python tla/tla.py 
    python semantisation/semantisation.py 
    python exploitation/exploitation.py 

Ainsi, les répertoires se créeront à tour de rôle dans le répertoire courant et seront accessibles par les autres modules.
_Si les commandes ne fonctionne pas, tester "python3 x.py" ou "python -m x.py"._

Les fichiers README ainsi que les commentaires présents dans le code vous aideront à utiliser les scripts des modules.

## UTILISATION AVEC AWS 
### Mise en oeuvre du module extraction et tla sur AWS 

Vous rendre dans le répertoire [Iaas_aws](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/Iaas_aws).

Renseigner le fichier terraform.tfvars avec vos keys.

Puis exécuter les commandes suivantes (dans l'ordre) pour créer les ressources et lancer la solution :

    terraform init

    terraform apply

Enfin pour détruire la solution et les ressources, exécuter la commande :

    terraform destroy


## UTILISATION AVEC KUBERNETES

### Pré-requis
- Avoir créé les buckets S3 suivants sur AWS :
    - bucket-extraction
    - bucket-tla

Configurer vos variables d'environnement avec les credentials (key aws et openAI).

- **Méthode 1 : AWS CLI**

Configurer vos credentials AWS avec la commande :

    aws configure

Si vous utilisez un compte AWS Academy, il vous faudra ajouter à la main le token de session à l'aide de la commande :

    vim ~/.aws/credentials

Vérifier que vos credentials soient bien pris en compte :

    aws sts get-caller-identity

- **Méthode 2 : variables d'environnement**

Insérer dans votre terminal les variables

    export aws_access_key_id=xxxxx
    export aws_secret_access_key=xxxxx
    export aws_session_token=xxxxx
    export open_api_key=xxxx

Veuillez remplacer par ce formalisme si vous êtes sur Windows 

    $Env:AWS_ACCESS_KEY_ID="xxxx"
    ...

- **Méthode 3 : renseigner les variables d'environnement en dur dans le script**

Pour cela, veuillez renseigner la partie "env" disponible dans le fichier .yaml

### Création de pods Kubernetes pour les modules extraction et tla 

**MODULE EXTRACTION** : 

  - Vous rendre dans le répertoire [extraction](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/extraction)

  - Créer un cluster avec la commande : 

        kind create cluster --name cluster-extraction

  - Créer les pods :

        kubectl apply -f extraction_pod.yaml

  - Vérifier que les pods soient bien déployés avec succès (jusqu'au statut running):

        kubectl get pods

Les répertoires présents dans les bucket S3 seront peuplés de fichiers JSON.

  - Pour supprimer tous les pods :

        kubectl delete pod --all

  - Pour supprimer le cluster :

        kind delete cluster --name cluster-extraction

**MODULE TLA** :

  - Vous rendre dans le répertoire [tla](https://gitlab-student.centralesupelec.fr/maelle.pariez/projet_academique_cs/-/tree/main/tla)

  - Créer un cluster avec la commande : 

        kind create cluster --name cluster-tla

  - Créer les pods :

        kubectl apply -f tla_pod.yaml

  - Vérifier que les pods soient bien déployés avec succès (jusqu'au statut running):

        kubectl get pods

Les répertoires présents dans les bucket S3 seront peuplés de fichiers JSON.

  - Pour supprimer tous les pods :

        kubectl delete pod --all

  - Pour supprimer le cluster :

        kind delete cluster --name cluster-tla

