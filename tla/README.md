# Génération des mots clés et du sujet de l'article d'Arxiv
Le module TLA a pour but de classer les documents arXiv issus du module extraction par thèmes. Cette classification se fera sur la base des méta-données de chaque pdf, le but étant de fournir en sortie un ou des fichiers json avec les éléments nécessaires à l’alimentation de l’ontologie qui aura été créée au préalable.

Le module comporte quatre étapes essentielles qui sont les suivantes :
- La génération des mots-clés (keywords) à partir du json d’entré et à l’aide dumodèle KeyBERT.
- La clusterisation des mots clés à l’aide de l’algorithme du Kmeans.
- La définition de thèmes grâce au modèle GPT.
- La génération des jsons de sortie en affectant à chaque document PDF son thème.


# Prérequis
Python 3.10

## Les packages Python suivants :
- keybert
- openai

# Utilisation avec docker
## Pré-requis 
- Configurer les accès AWS

## Création et lancement du docker tla
        docker build -t tla .
        ocker run -p 6000:6000 tla

## Lancement de pods Kubernetes
### Pré-requis
- Avoir créé les buckets S3 suivants sur AWS :
    - bucket-extraction
    - bucket-tla
Si vous ne le faîtes pas, vous rencontrerez une erreur au moment du peuplement des buckets et lors de la récupération des objets situés sur AWS (buckets).
- Configurer vos credentials AWS et OpenAI (fichier tla_pod.yaml)

### Utilisation avec Kubernetes
Pour créer un cluster :

        kind create cluster --name cluster-tla

Pour créer un pod :

        kubectl apply -f tla_pod.yaml

Pour voir si les pods sont déployés avec succès :

        kubectl get pods

Pour supprimer tous les pods :

        kubectl delete pod --all

Pour supprimer le cluster :

        kind delete cluster --name cluster-tla

# Utilisation sans docker

- Clonez ce dépôt et naviguez jusqu'au dossier contenant le script tla.py.

- Pour exécuter le script en local, enlever les commentaires situés devant les fonctions :
    - keywords_generator_local()
    - cluster_function_local()

- Pour exécuter le script sur AWS (en utilisant les buckets) :
    - Enlever les commentaires situés devant les fonctions keywords_generator_S3() et cluster_function_S3()
    - Paramétrer vos variables d'environnement openAI (et AWS si vous souhaitez utiliser les buckets).
        - Sur Linux :

                        export aws_access_key_id=xxxxx
                        export aws_secret_access_key=xxxxx
                        export aws_session_token=xxxxx
                        export open_api_key=xxxx

        - Sur Windows :

                        $Env:AWS_ACCESS_KEY_ID="xxxx"
                        $Env:AWS_SECRET_ACCESS_KEY="xxxx"
                        $Env:AWS_SESSION_TOKEN="xxxx"
                        $Env:AWS_DEFAULT_REGION="us-east-1"
                        $Env:OPENAI_API_KEY="xxxx"

- Exécutez le script tla.py à l'aide de la commande python tla.py.

- Les fichiers JSON contenant les mots clés seront stockés dans le répertoire jsons_with_keywords, et les fichiers JSON contenant les sujets seront stockés dans le répertoire jsons_with_subject.

# Fonctionnalités
Le script est divisé en deux fonctions principales : keywords_generator et cluster_function (avec à chaque fois une version en local et une version sur aws).


## keywords_generator
Cette fonction extrait les mots clés des fichiers PDF à l'aide de KeyBERT. Le mot-clé le plus pertinent (celui qui obtient le score le plus fort) est retenu. Les fichiers JSON contenant les keywords sont stockés dans le répertoire jsons_with_keywords.

## cluster_function
Cette fonction détermine le sujet de l'article à partir du mot clé qui a été déterminé par la fonction précédente. Pour cela, un procédé de clusterisation est utilisé et l'API de ChatGPT est utilisé pour donner un nom de thématique à chaque cluster. Les fichiers JSON contenant les métadonnées, les liens connexes et les sujets (thématique principale) des articles sont stockés dans le répertoire jsons_with_subject. C'est ce fichier qui sera utilisé par le module suivant (sémantisation).

# Gestion des erreurs 
Si vous obtenez le message KeyError: 'OPENAI_API_KEY', c'est que vous n'avez pas défini la variable d'environnement correspondant à votre Key OpenAI.

La commande sur Linux :

        export open_api_key=xxxx

La commande sur Windows :

        $Env:AWS_ACCESS_KEY_ID="xxxx"