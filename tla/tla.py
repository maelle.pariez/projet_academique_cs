import json
import os
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import openai
from keybert import KeyBERT
import boto3

# Create an S3 client
s3 = boto3.client('s3')

# Specify the bucket name to store the file
bucket_name_in = 'bucket-extraction'
bucket_name_out = 'bucket-tla'
directory_in = 'jsons_with_metadata_and_related_links/'
directory_out_keywords = "jsons_with_keywords/"
directory_out_subject = "jsons_with_subject/"

#directory = "jsons_with_keywords/"


def keywords_generator_S3() :
    print("********* Start keywords generating *********")

    if not os.path.exists(directory_out_keywords):
        os.mkdir(directory_out_keywords)

    kw_model = KeyBERT("distilbert-base-nli-mean-tokens")

    # Récupérer la liste des objets dans le bucket
    response = s3.list_objects_v2(Bucket=bucket_name_in, Prefix=directory_in)

    # Loop à travers les objets dans le bucket
    for obj in response['Contents']:
        key = obj['Key']

        # Vérifier si l'objet est un fichier JSON
        if key.endswith('.json'):
            # Télécharger le contenu du fichier JSON depuis S3
            response = s3.get_object(Bucket=bucket_name_in, Key=key)
            json_content = response['Body'].read()

            # Convertir le JSON en objet python
            data = json.loads(json_content)
            
            # dictionary to avoid duplicates
            dict_documents = {}
            
            # Traitement des données
            for item in data:
                # Extract keywords from the summary
                summary = item['summary']
                keywords = kw_model.extract_keywords(summary, top_n=1, keyphrase_ngram_range=(1, 2), stop_words='english')
                item['keywords'] = keywords
            
            # Écrire les données traitées dans un nouveau fichier JSON dans le bucket S3
            new_key = directory_out_keywords + key.split('/')[-1]
            s3.put_object(Bucket=bucket_name_out, Key=new_key, Body=json.dumps(data))

    print("********* End of keywords generating *********")

            
def keywords_generator_local() :

    print("********* Start keywords generating *********")

    if not os.path.exists(directory_out_keywords):
        os.mkdir(directory_out_keywords)

    kw_model = KeyBERT("distilbert-base-nli-mean-tokens")

    # Loop through all the files in the json directory
    for filename in os.listdir('jsons_with_metadata_and_related_links'):

        print("********* Step : "+ filename+ " *********")
        if filename.endswith('.json'):
            filepath = os.path.join('jsons_with_metadata_and_related_links', filename)
            
            
            # Load the JSON file
            with open(filepath, 'r') as f:
                data = json.load(f)
                
            for item in data:
                # Extract keywords from the summary
                summary = item['summary']
                keywords = kw_model.extract_keywords(summary, top_n=1, keyphrase_ngram_range=(1, 2), stop_words='english')
                item['keywords'] = keywords

                print("Keywords : ")
                print(keywords)

        with open(directory_out_keywords+filename, 'w') as f:
            json.dump(data, f)

    print("********* End of keywords generating *********")



def cluster_function_S3() : 

    print("********* Start of clustering *********")

    # Clé APIde OpenAI à renseigner. Ici, c'est un export depuis l'OS qui a été utilisé.
    openai.api_key = os.environ["OPENAI_API_KEY"] 

    pd.set_option('display.max_rows', None)

    # Création d'une liste de keywords
    liste_de_key_words = []

    # Récupérer la liste des objets dans le bucket
    response = s3.list_objects_v2(Bucket=bucket_name_out, Prefix=directory_out_keywords)

    # Loop à travers les objets dans le bucket
    for obj in response['Contents']:
        key = obj['Key']

        # Vérifier si l'objet est un fichier JSON
        if key.endswith('.json'):
            # Télécharger le contenu du fichier JSON depuis S3
            response = s3.get_object(Bucket=bucket_name_out, Key=key)
            json_content = response['Body'].read()

            # Convertir le JSON en objet python
            data = json.loads(json_content)
                
            # Traitement des données
            for item in data:
            # Extrait les mots clés du 
                for value in range(len(item["keywords"])):
                    liste_de_key_words.append(item["keywords"][value][0])


    # On fait un set pour supprimer les redondances
    keywords = list(set(liste_de_key_words))

    # Clusterisation
    # Convertir la liste de keywords en dataframe
    keywords = pd.DataFrame({"mots": keywords})

    # Convertir les mots-clés en vecteurs Tfidf
    vectorizer = TfidfVectorizer(stop_words="english")
    X = vectorizer.fit_transform(keywords["mots"])

    # Appliquer l'algorithme K-Means pour créer 10 clusters
    kmeans = KMeans(n_clusters=10, random_state=42, n_init=10, algorithm='elkan').fit(X)


    # Ajouter les clusters au dataframe
    keywords["cluster"] = kmeans.labels_


    # Créer une liste contenant les clusters distincts triés
    clusters = np.sort(keywords["cluster"].unique())

    # Pour chaque cluster, envoyer une requête à GPT-3 pour générer un nom de thème
    topics = []

    # On détermine les thèmes à partir d'un échantillon de 1500. On ne peut pas faire plus à cause des limitations de ChatGPT
    df = keywords[0:1400]
    for cluster in clusters:
        # Sélectionner les mots appartenant à ce cluster
        cluster_words = df.loc[df["cluster"] == cluster, "mots"].tolist()

        # Créer une chaîne de caractères contenant les mots séparés par des virgules
        prompt = f"Cluster {cluster}: {', '.join(cluster_words)}.\n  Donne moi un nom de thème pour ce cluster en deux mots et en anglais et qui soit différents des autres : "

        # Envoyer la requête à GPT-3
        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            max_tokens=20,
            n=20,
            stop=None,
            temperature=0.7,
        )

        resultat = response.choices[0].text.rstrip(' .').strip()
        topics.append(resultat)
            

    # On regroupe par topic
    keywords['topic'] = keywords['cluster'].map(lambda x: topics[x])
    grouped = keywords.groupby('topic')


    # On boucle sur les jsons pour remplir les jsons par les topics et selon le formalisme voulu
    # directory_out_subject = "jsons_with_subject/"
    if not os.path.exists(directory_out_subject):
        os.mkdir(directory_out_subject)

    # Récupérer la liste des objets dans le bucket
    response = s3.list_objects_v2(Bucket=bucket_name_out, Prefix=directory_out_keywords)

    # Loop à travers les objets dans le bucket
    for obj in response['Contents']:
        key = obj['Key']

        # Vérifier si l'objet est un fichier JSON
        if key.endswith('.json'):
            # Télécharger le contenu du fichier JSON depuis S3
            response = s3.get_object(Bucket=bucket_name_out, Key=key)
            json_content = response['Body'].read()

            # Convertir le JSON en objet python
            data = json.loads(json_content)
                
            i=0
            new_data_list = []
            # Traitement des données
            for item in data:
                sujet = "vide"
                for groupe, df_groupe in grouped:
                    if item["keywords"][0][0] in df_groupe['mots'].values:
                        sujet = groupe
                        break

                link = item["link"]
                id_str = link.split("/")[-1]  # Sélectionne la dernière partie de l'URL
                id_str = id_str.replace(".pdf", "")  # Retire l'extension '.pdf'
                id_str = id_str.replace(".", "-")  # Remplace le point par un tiret
                id_str = id_str.replace("v1", "")  # Retire la chaîne "v1"
                id_str = id_str.replace("v2", "")  # Retire la chaîne "v2"
                id_str = id_str.replace("v3", "")  # Retire la chaîne "v3"
                id_str = id_str.replace("v4", "")  # Retire la chaîne "v4"
                # print(id_str)  # Affiche le format type : '2303-17591'

                new_data = {
                'title' : item["title"],  
                'subject' : sujet,
                'created' : item["published"],
                'identifier' : id_str,
                'bibliographicCitation' : None,
                'maker' : [{'name' : auteur } for auteur in item["authors"] ]
                            }
                new_data_list.append(new_data)
                
            # Écrire les données traitées dans un nouveau fichier JSON dans le bucket S3
            new_key = directory_out_subject + key.split('/')[-1]
            s3.put_object(Bucket=bucket_name_out, Key=new_key, Body=json.dumps(new_data_list))


    print("********* End of clustering *********")

    print("********** Jsons with Subject added ***********")


def cluster_function_local() : 

    print("********* Start of clustering *********")

    # Clé APIde OpenAI à renseigner. Ici, c'est un export depuis l'OS qui a été utilisé.
    openai.api_key = os.environ["OPENAI_API_KEY"] 

    pd.set_option('display.max_rows', None)

    # Création d'une liste de keywords
    liste_de_key_words = []

    # Boucle sur tous les fichiers dans le répertoire json
    for filename in os.listdir("jsons_with_keywords"):

        if filename.endswith(".json"):
            filepath = os.path.join(directory_out_keywords, filename)

            # Charge le fichier JSON 
            with open(filepath, "r") as f:
                data = json.load(f)

            for item in data:
                # Extrait les mots clés du 

                for value in range(len(item["keywords"])):
                    liste_de_key_words.append(item["keywords"][value][0])

    # On fait un set pour supprimer les redondances
    keywords = list(set(liste_de_key_words))

    # Clusterisation
    # Convertir la liste de keywords en dataframe
    keywords = pd.DataFrame({"mots": keywords})

    # Convertir les mots-clés en vecteurs Tfidf
    vectorizer = TfidfVectorizer(stop_words="english")
    X = vectorizer.fit_transform(keywords["mots"])

    # Appliquer l'algorithme K-Means pour créer 2 clusters
    kmeans = KMeans(n_clusters=2, random_state=42, n_init=10, algorithm='elkan').fit(X)


    # Ajouter les clusters au dataframe
    keywords["cluster"] = kmeans.labels_


    # Créer une liste contenant les clusters distincts triés
    clusters = np.sort(keywords["cluster"].unique())

    # Pour chaque cluster, envoyer une requête à GPT-3 pour générer un nom de thème
    topics = []

    # On détermine les thèmes à partir d'un échantillon de 1500. On ne peut pas faire plus à cause des limitations de ChatGPT
    df = keywords[0:1400]
    for cluster in clusters:
        # Sélectionner les mots appartenant à ce cluster
        cluster_words = df.loc[df["cluster"] == cluster, "mots"].tolist()

        # Créer une chaîne de caractères contenant les mots séparés par des virgules
        prompt = f"Cluster {cluster}: {', '.join(cluster_words)}.\n  Donne moi un nom de thème pour ce cluster en deux mots et en anglais et qui soit différents des autres : "

        # Envoyer la requête à GPT-3
        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            max_tokens=20,
            n=20,
            stop=None,
            temperature=0.7,
        )

        resultat = response.choices[0].text.rstrip(' .').strip()
        topics.append(resultat)
        
        print("résultat : ")
        print(resultat)

    # On regroupe par topic
    keywords['topic'] = keywords['cluster'].map(lambda x: topics[x])
    grouped = keywords.groupby('topic')


    # On boucle sur les jsons pour remplir les jsons par les topics et selon le formalisme voulu
    directory_out_subject = "jsons_with_subject/"
    if not os.path.exists(directory_out_subject):
        os.mkdir(directory_out_subject)

    for filename in os.listdir('jsons_with_keywords'):
        if filename.endswith('.json'):
            filepath = os.path.join('jsons_with_keywords', filename)
                
            # Load the JSON file
            with open(filepath, 'r') as f:
                data = json.load(f)
                i=0
                new_data_list = []
            for item in data:
                sujet = "vide"
                for groupe, df_groupe in grouped:
                    if item["keywords"][0][0] in df_groupe['mots'].values:
                        sujet = groupe
                        break
                
                # print(item["link"])
                link = item["link"]
                id_str = link.split("/")[-1]  # Sélectionne la dernière partie de l'URL
                id_str = id_str.replace(".pdf", "")  # Retire l'extension '.pdf'
                id_str = id_str.replace(".", "-")  # Remplace le point par un tiret
                id_str = id_str.replace("v1", "")  # Retire la chaîne "v1"
                id_str = id_str.replace("v2", "")  # Retire la chaîne "v2"
                id_str = id_str.replace("v3", "")  # Retire la chaîne "v3"
                id_str = id_str.replace("v4", "")  # Retire la chaîne "v4"
                # print(id_str)  # Affiche le format type : '2303-17591'
                

                new_data = {
                'title' : item["title"],  
                'subject' : sujet,
                'created' : item["published"],
                'identifier' : id_str,
                'bibliographicCitation' : None,
                'maker' : [{'name' : auteur } for auteur in item["authors"] ]
                        }
                new_data_list.append(new_data)
            
        with open(directory_out_subject+filename, 'w') as f:
            json.dump(new_data_list, f)

    print("********* End of clustering *********")

    print("********** Jsons with Subject added ***********")


# Exécution avec Bucket S3 : enlever les commentaires pour exécuter la fonction avec AWS
#keywords_generator_S3()
#cluster_function_S3()

# Exécution en local : enlever les commentaires pour exécuter la fonction en local
keywords_generator_local()
cluster_function_local()

# Delete inutiles folders from local
# shutil.rmtree("jsons_with_keywords")
# shutil.rmtree("jsons_with_subject")