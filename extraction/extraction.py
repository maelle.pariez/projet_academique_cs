import requests
from bs4 import BeautifulSoup
import json
import os
import re
import PyPDF2
from io import BytesIO
import boto3
import shutil
import time


# Create an S3 client
s3 = boto3.client('s3')

# Specify the bucket name to store the file
bucket_name = 'bucket-extraction'


def extract_metadata(counter) :

    print("********* Start downloading metadata *********")

    # Define the URL template for the API
    url_template = "http://export.arxiv.org/api/query?search_query=cat:cs.AI&start={}&max_results=1000&sortBy=submittedDate&sortOrder=descending"
    directory = "jsons_with_metadata/"

    # Create jsons_with_metadata directory if it does not exist"
    if not os.path.exists("jsons_with_metadata"):
        os.mkdir("jsons_with_metadata")
   # else:
    #    os.remove("jsons_with_metadata")

    def extract_pdf_links(start_index,file_name):
        
        # Create the URL for the given start index
        url = url_template.format(start_index)
        # Send a GET request to the URL and parse the response with BeautifulSoup
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "xml")
        # Find all the "entry" elements in the response
        entries = soup.find_all("entry")
        metadata_list = []

        # Iterate over each entry and extract the desired metadata
        for entry in entries:
            metadata = {}
            
            # Find the link to the PDF file
            links = entry.find_all("link")
            for link in links:
                if link.get("title") == "pdf":
                    metadata["link"] = link.get("href")
                    #id_link = link.get("href")
                    #id_regex = re.compile(r'http[s]?://arxiv\.org/(?:pdf|abs)/(\d+\.\d+(?:v\d+)?)')
                    #id_arxiv = id_regex.findall(id_link)
                    #metadata["link"]=id_arxiv[0].replace(".", "-").split("v")[0]
                    
            # Find the title of the paper
            metadata["title"] = entry.find("title").text.strip()
            
            # Find the summary of the paper
            metadata["summary"] = entry.find("summary").text.strip()
            
            # Find the names of the authors of the paper
            authors = entry.find_all("author")
            author_names = [author.find("name").text.strip() for author in authors if author.find("name")]
            metadata["authors"] = author_names
            
            # Find the date the paper was last updated
            metadata["updated"] = entry.find("updated").text.strip()

            # Find the date the paper was originally published
            metadata["published"] = entry.find("published").text.strip()
            
            # Add the metadata to the list
            metadata_list.append(metadata)

        # Write the metadata list to a JSON file
        # with open(file_name, "w", encoding='utf-8') as f:
        with open(file_name, "w") as f:
            json.dump(metadata_list, f)
        
        # Print progress information
        # print("Downloaded metadata for entries {}-{}".format(start_index, start_index + len(entries) - 1))
        print("Downloaded metadata for entries test {}-{}".format(start_index+1, start_index + 1000))
        # Return the metadata list
        return metadata_list

    # Define the starting index for the API query
    start_index = 0

    # Keep looping until we have downloaded metadata for all entries
    file_name = directory + "from_1_to_1000.json"

    counter = counter*1000

    while start_index < counter:
        
        extract_pdf_links(start_index,file_name)
        # Increment the start index for the next batch of entries
        start_index += 1000
        
        # Update the file name for the next batch of entries
        file_name = directory + f"from_{start_index+1}_to_{start_index+1000}.json"


    # Print a message indicating that we're done
    print("********* Finished downloading metadata *********")



def extract_related_links(counter=None) : 

    print("********* Start downloading related_links *********")

    # Define the directory for saving updated JSON files
    directory = "jsons_with_metadata_and_related_links/"
    
    # Create the directory if it doesn't already exist
    if not os.path.exists("jsons_with_metadata_and_related_links"):
        os.mkdir("jsons_with_metadata_and_related_links")
    

    # Iterate over all JSON files in the 'jsons_with_metadata' directory
    for filename in os.listdir('jsons_with_metadata'):

        if filename.endswith('.json'):
            filepath = os.path.join('jsons_with_metadata', filename)
            
            # To save the data of used pdfs
            used_data = []

            # Iterate over each paper for user logs
            log_counter = 1
            
            # Load the JSON file
            with open(filepath, encoding='cp1252', errors='ignore') as f:
            #with open(filepath, encoding='utf-8') as f:
            #with open(filepath, 'r') as f:
                data = json.load(f)
            
            # Set a counter variable to limit the loop if provided
            if counter is None:
                counter = len(data)
            else:
                counter = min(counter, len(data))
            
            # Iterate over each paper in the JSON file
            for paper in data[:counter]:
                
                pdf_url = paper['link']
                
                # Download the PDF from the URL
                response = requests.get(pdf_url)
                pdf_file = BytesIO(response.content)
                
                # Extract the text from the PDF
                pdf_reader = PyPDF2.PdfReader(pdf_file)
                text = ''
                for page in pdf_reader.pages:
                    text += page.extract_text()
                
                # Find all links in the text using a regular expression
                link_regex = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
                links = link_regex.findall(text)

                # Add the links to the paper dictionary
                paper['related_links'] = links

                # Add the IDs to the paper dictionary
                #id_regex = re.compile(r'http[s]?://arxiv\.org/(?:pdf|abs)/(\d+\.\d+(?:v\d+)?)')
                #links_arxiv = id_regex.findall(text)
                #links_arxiv = id_regex.findall(text).replace(".pdf", "")  # Retire l'extension '.pdf'
                #links_arxiv = id_regex.findall(text).replace(".", "-") # Remplace le point par un trait de soulignement
                #links_arxiv = id_regex.findall(text).replace("v1", "").replace("v2", "").replace("v3", "").replace("v4", "") # Retire la chaîne "v1, v2, v3, v4"
                id_regex = re.compile(r'http[s]?://arxiv\.org/(?:pdf|abs)/(\d+\.\d+(?:v\d+)?)')
                links = id_regex.findall(text)
                
                for i in range(len(links)):
                    links[i] = links[i].replace(".", "-")
                    if 'v' in links[i]:
                        links[i] = links[i].split('v')[0]
                
                ids = [link.split('/')[-1] for link in links]
                paper['related_ids'] = ids
                
                
                # Append the paper data to the 'used_data' list
                used_data.append(paper)
                print(f"First_{log_counter}_"+filename)
                log_counter += 1
                                
            # Respect the arXiv's directive for requests : each 4 seconds
            time.sleep(4)

            # Save the updated data into a new JSON file in the 'jsons_with_metadata_and_related_links' directory
            filename =f"First_{counter}_"+filename
            #with open(directory+filename, 'w', encoding='utf-8') as f:
            with open(directory+filename, 'w') as f:
                json.dump(used_data, f)
            

            def save_on_bucket():
                filenames3 = directory + filename

                # Upload the file to the bucket
                s3.upload_file(filenames3, bucket_name, filenames3)
                # s3.upload_file(directory + filename, bucket_name, filename)

                # Print a message indicating that we're done
                print("Data with .json on Bucket")
            
            # Pour enregistrer le répertoire et les fichiers sur un bucket S3, enlever le commentaire et exécuter la commande ci-dessous
            # save_on_bucket()

    print("********* Finished downloading related_links *********")

# Extract metadata from arxiv,the counter is used to limit the number of json files. Each Json file contains 1000 pdf
extract_metadata(counter=1)
# ----------- Vous pouvez choisir dans counter un nombre situé entre 1 et 35 : 1 pour 1 fichier de 1000 PDF, 35 pour 35 fichiers de 1000 PDF  --------- #
# ----------- Pour effectuer des tests dans un temps raisonnable, je vous recommande de ne pas dépasser 5  --------- #


# Extract related links of each pdfs, the counter is used to limit the number of pdfs
extract_related_links(counter=2)
# ----------- Vous pouvez choisir dans counter un nombre situé entre 1 et 1000 : 1 pour lire le 1er lien de chaque fichier ou 1000 pour lire tous les liens de chaque fichier  --------- #
# ----------- Pour effectuer des tests dans un temps raisonnable, je vous recommande de ne pas dépasser 10  --------- #


# Delete inutiles folders from local
# shutil.rmtree("jsons_with_metadata")
# shutil.rmtree("jsons_with_metadata_and_related_links")