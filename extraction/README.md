# Extraction de métadonnées et de liens connexes à partir de fichiers PDF d'Arxiv
Ce script permet de télécharger des métadonnées et des liens connexes à partir de fichiers PDF hébergés sur le site Arxiv.org.

# Prérequis
Python 3.10

## Les packages Python suivants :
- requests
- BeautifulSoup
- json
- PyPDF2

# Utilisation avec docker
## Pré-requis 
- Configurer les accès AWS

## Création et lancement du docker extraction
        docker build -t extraction .
        docker run -p 4000:4000 extraction

## Lancement de pods Kubernetes
### Pré-requis
- Avoir créé les buckets S3 suivants sur AWS :
    - bucket-extraction
    - bucket-tla
Si vous ne le faîtes pas, vous rencontrerez une erreur au moment du peuplement des buckets et lors de la récupération des objets situés sur AWS (buckets).
- Configurer vos credentials AWS dans le fichier extraction_pod.yaml

### Utilisation avec Kubernetes
Pour créer un cluster :
                
        kind create cluster --name cluster-extraction

Pour créer un pod :
                
        kubectl apply -f extraction_pod.yaml

Pour voir si les pods sont déployés avec succès :
                
        kubectl get pods

Pour supprimer tous les pods :
                
        kubectl delete pod --all

Pour supprimer le cluster :
                
        kind delete cluster --name cluster-extraction

# Utilisation sans docker
Clonez ce dépôt et naviguez jusqu'au dossier contenant le script.
Exécutez le script extraction.py à l'aide de la commande python extraction.py.
Les fichiers JSON contenant les métadonnées seront stockés dans le répertoire jsons_with_metadata, et les fichiers JSON contenant les liens connexes seront stockés dans le répertoire jsons_with_metadata_and_related_links.

## Utilisation avec AWS
Si vous souhaitez enregistrer ces données sur un bucket S3 AWS (et que vos credentials sont valides), veuillez enlever le commentaire permettant d'exécuter la fonction save_on_bucket() et paramétrer vos variables d'environnement AWS pour enregistrer les JSON sur le bucket-extraction.

- Sur Linux :

        export aws_access_key_id=xxxxx
        export aws_secret_access_key=xxxxx
        export aws_session_token=xxxxx

- Sur Windows :

        $Env:AWS_ACCESS_KEY_ID="xxxx"
        $Env:AWS_SECRET_ACCESS_KEY="xxxx"
        $Env:AWS_SESSION_TOKEN="xxxx"
        $Env:AWS_DEFAULT_REGION="us-east-1"


# Fonctionnalités
Le script est divisé en deux fonctions principales : extract_metadata et extract_related_links.

## extract_metadata
Cette fonction extrait les métadonnées des fichiers PDF en utilisant l'API Arxiv. Les fichiers JSON contenant les métadonnées sont stockés dans le répertoire jsons_with_metadata.

## extract_related_links
Cette fonction extrait les liens connexes à partir des fichiers PDF en utilisant une expression régulière. Les fichiers JSON contenant les métadonnées et les liens connexes sont stockés dans le répertoire jsons_with_metadata_and_related_links. C'est ce fichier qui sera utilisé par le module suivant (tla).