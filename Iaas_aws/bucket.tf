resource "aws_s3_bucket" "bucket-extraction" {
  bucket = "bucket-extraction"
#  acl = "public-read-write"
  force_destroy = true
}

resource "aws_s3_bucket" "bucket-tla" {
  bucket = "bucket-tla"
#  acl    = "public-read-write"
  force_destroy = true
}

resource "aws_s3_bucket" "bucket-semantisation" {
  bucket = "bucket-semantisation"
#  acl    = "public-read-write"
  force_destroy = true
}
