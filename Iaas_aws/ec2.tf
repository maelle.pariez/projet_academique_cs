terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.76.1"
    }
  }
}

provider "aws" {
  # profile = "default"
  region     = "us-east-1"
}


variable "openai_api_key" {}

variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "aws_session_token" {}


provider "docker" {}

resource "aws_ebs_volume" "additional_storage" {
  availability_zone = "us-east-1a"
  size              = 50
}

resource "aws_instance" "docker_host" {
    ami           = "ami-007855ac798b5175e"
    instance_type = "m5.large"

    ebs_block_device {
        device_name = "/dev/xvdf"
        volume_size = 50
        volume_type = "gp3"
    }

    depends_on = [
        aws_ebs_volume.additional_storage
    ]
    
    user_data = <<-EOF
        #!/bin/bash
        # Mettre à jour les paquets existants
        sudo apt-get update

        # Installer AWS CLI
        sudo apt install awscli

        # Installer Git
        sudo apt-get install git -y

        # Installer Docker
        curl -fsSL https://get.docker.com -o get-docker.sh
        sudo sh get-docker.sh

        # Ajouter l'utilisateur courant au groupe Docker pour éviter de devoir utiliser sudo à chaque fois
        sudo usermod -aG docker $USER

        sudo usermod -a -G docker ubuntu
        id ubuntu
        newgrp docker

        sudo systemctl enable docker.service
        sudo systemctl start docker.service

        # Installer Kubernetes
        sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
        curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
        echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
        sudo apt-get update
        sudo apt-get install -y kubectl

        # Installer Kind
        curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64
        chmod +x ./kind
        sudo mv ./kind /usr/local/bin

        # Installer Terraform
        sudo apt-get install -y unzip
        wget https://releases.hashicorp.com/terraform/1.4.4/terraform_1.4.4_linux_amd64.zip
        unzip terraform_1.4.4_linux_amd64.zip
        sudo mv terraform /usr/local/bin/

        docker run -d -p 8000:80 \
        -e "AWS_ACCESS_KEY_ID=${var.aws_access_key}" \
        -e "AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}" \
        -e "AWS_SESSION_TOKEN=${var.aws_session_token}" \
        ${docker_container.extraction.image}

        docker run -d -p 8001:80 \
        -e "AWS_ACCESS_KEY_ID=${var.aws_access_key}" \
        -e "AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}" \
        -e "AWS_SESSION_TOKEN=${var.aws_session_token}" \
        ${docker_container.tla.image}

    EOF

}

resource "docker_container" "extraction" {
    name  = "extraction"
    image = "maellep/extraction:latest"
    env = [
    "AWS_ACCESS_KEY_ID=${var.aws_access_key}",
    "AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}",
    "AWS_SESSION_TOKEN=${var.aws_session_token}"
    ]
}

resource "docker_container" "tla" {
    depends_on = [ "docker_container.extraction" ]
    name  = "tla"
    image = "maellep/tla:latest"
    env = [
    "AWS_ACCESS_KEY_ID=${var.aws_access_key}",
    "AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}",
    "AWS_SESSION_TOKEN=${var.aws_session_token}",
    "OPENAI_API_KEY=${var.openai_api_key}"
    ]
}

