# Lister les requêtes
curl --location '127.0.0.1:5000/sparql/list_queries'

# Voir une requête
curl --location '127.0.0.1:5000/sparql/view_query?query_name=nb_docs'

# Exécuter une requête enregistrée
curl --location '127.0.0.1:5000/sparql/query' \
--form 'query_name="nb_docs"'

# Supprimer une requête
curl --location '127.0.0.1:5000/sparql/query' \
--form 'query_name="nb_docs"' \
--form 'query_remove="True"'

# Enregistrer une requête
curl --location '127.0.0.1:5000/sparql/query' \
--form 'query="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sio: <http://www.semanticweb.org/lox/ontologies/2023/ontology-sio#>
SELECT (COUNT(*) as ?count)
WHERE {{
?doc rdf:type sio:Document .
}}"' \
--form 'query_name="nb_docs"' \
--form 'query_save="True"' \
--form 'query_desc="Compter le nombre de documents"'

