"""
Owl controller

This module manages the ontology

Functions:
     - json2owl(json_file, owl_file, reasoner = True): complete the ontology
        from the json file
     - launch_reasoner(onto): launch reasoner
"""
import json
from datetime import datetime
import boto3
import os
import rdflib

from owlready2 import (
    get_ontology,)


# Create an S3 client
s3 = boto3.client('s3')

# Specify the bucket name to store the file
bucket_name_in = 'bucket-tla'
bucket_name_out = 'bucket-semantisation'
directory_in = 'jsons_with_subject/'
directory_out = 'onto/'

if not os.path.exists(directory_out):
            os.mkdir(directory_out)

print("********* Start Creating Ontology and generating files *********")


#def create_onto():

# create a new RDF graph
g = rdflib.Graph()

# define the namespace for the ontology
onto_ns = rdflib.Namespace('http://www.semanticweb.org/lox/ontologies/2023/ontology-sio#')

# add the ontology namespace to the graph
g.bind('onto', onto_ns)

OWL_PATH = 'model/sio-ontologie.owl'
sio = get_ontology(OWL_PATH).load()

print("Ontologie : " + onto_ns)

def launch_reasoner(onto):
    """
    launch reasoner
    :param onto: ontology
    :return: true if no inconsistencies
    """
    try:
        sync_reasoner(x=onto, infer_property_values=True, debug=True,
                      keep_tmp_file=True)
        result = True
    except OwlReadyInconsistentOntologyError():
        result = False
    finally:
        return result

def json2owl_local(reasoner=False):
    """
    complete the ontology from the json file in local without bucket S3
    :param json_file:
    :return: True if ok
    """
    # dictionary to avoid duplicates
    dict_documents = {}

    # Iterate over all JSON files in the 'jsons_with_metadata' directory
    for filename in os.listdir('jsons_with_subject'):
        if filename.endswith('.json'):
            filepath = os.path.join('jsons_with_subject', filename)
            
            # Load the JSON file
            #with open(filepath, encoding='cp1252', errors='ignore') as f:
            with open(filepath, 'r') as f:
                data = json.load(f)

            # Traitement des données
            for item in data:

                identifier = item['identifier']
                print("identifier : ")
                print(identifier)

                if identifier in dict_documents:
                    document = dict_documents[identifier]
                else:
                    document = sio.Document()
                    if identifier is None:
                        document.identifier.append("unknown")
                    else:
                        document.identifier.append(identifier)
                    dict_documents[identifier] = document

                document.title.append(item['title'])
                document.subject.append(item['subject'])

                created_str = item['created'][:-1]  # Enlève le suffixe 'Z'
                document.created.append(
                    datetime.fromisoformat(created_str)
                )
                #document.created.append(
                #    datetime.fromisoformat(data['created'])
                #)

                
                # adding authors
                for agent in item['maker']:
                    name = agent['name'].replace(' ', '_').replace('.', '').replace("'", "")
                    #name = rdflib.URIRef(agent['name'].replace("'", "").replace(".", "").replace(' ', '_'))

                    #name = rdflib.URIRef(agent['name'].replace(' ', '_'))
                    author = sio.Agent(
                        name=name
                    )

                    document.maker.append(author)
                    author.made.append(document)

                # adding citations
                if 'bibliographicCitation' in item:
                    citations = item['bibliographicCitation']
                    if citations is not None:
                        for citation in citations:
                            citation_id = citation

                            if citation_id in dict_documents:
                                citation_doc = dict_documents[citation_id]
                            else:
                                citation_doc = sio.Document()
                                if citation_id is None:
                                    citation_doc.identifier.append("unknown")
                                else:
                                    citation_doc.identifier.append(citation_id)
                                dict_documents[citation_id] = citation_doc
                            document.bibliographicCitation.append(citation_doc)                          

            
            # loop through the documents and add them to the graph
            for document in dict_documents.values():

                # create a new RDF resource for the document
                #doc_res = onto_ns[document.identifier[0]]

                doc_id = document.identifier[0].replace("'", "%27")
                doc_res = onto_ns[doc_id]

                # add the document properties to the resource
                g.add((doc_res, rdflib.RDF.type, onto_ns['Document']))
                if len(document.title) > 0:
                    g.add((doc_res, onto_ns['title'], rdflib.Literal(document.title[0])))

                if len(document.subject) > 0:
                    g.add((doc_res, onto_ns['subject'], rdflib.Literal(document.subject[0])))

                if len(document.created) > 0:
                    g.add((doc_res, onto_ns['created'], rdflib.Literal(document.created[0], datatype=rdflib.XSD.dateTime)))

                # add the authors to the resource
                for author in document.maker:
                    author_res = onto_ns[author.name]
                    g.add((author_res, rdflib.RDF.type, onto_ns['Agent']))
                    g.add((author_res, onto_ns['made'], doc_res))
                    g.add((doc_res, onto_ns['maker'], author_res))

                # add the bibliographic citations to the resource
                for citation_doc in document.bibliographicCitation:
                    citation_res = onto_ns[citation_doc.identifier[0]]
                    g.add((doc_res, onto_ns['bibliographicCitation'], citation_res))

            # save the graph to a file
            g.serialize(destination='onto/my_ontology_sio.owl', format='xml')

    return sio

def json2owls3(reasoner=False):
    """
    complete the ontology from the json file with bucket S3
    :param json_file:
    :return: True if ok
    """
    # dictionary to avoid duplicates
    dict_documents = {}

    # Récupérer la liste des objets dans le bucket
    response = s3.list_objects_v2(Bucket=bucket_name_in, Prefix=directory_in)

    # Loop à travers les objets dans le bucket
    for obj in response['Contents']:
        key = obj['Key']

        # Vérifier si l'objet est un fichier JSON
        if key.endswith('.json'):
            # Télécharger le contenu du fichier JSON depuis S3
            response = s3.get_object(Bucket=bucket_name_in, Key=key)
            json_content = response['Body'].read()

            # Convertir le JSON en objet python
            data = json.loads(json_content)

            # Traitement des données
            for item in data:

                identifier = item['identifier']

                if identifier in dict_documents:
                    document = dict_documents[identifier]
                else:
                    document = sio.Document()
                    document.identifier.append(identifier)
                    dict_documents[identifier] = document

                document.title.append(item['title'])
                document.subject.append(item['subject'])

                created_str = item['created'][:-1]  # Enlève le suffixe 'Z'
                document.created.append(
                    datetime.fromisoformat(created_str)
                #    datetime.fromisoformat(item['created'])
                )

                # adding authors
                for agent in item['maker']:
                    name = agent['name'].replace(' ', '_')

                    author = sio.Agent(
                        name=name
                    )

                    document.maker.append(author)
                    author.made.append(document)

                # adding citations
                if 'bibliographicCitation' in item:
                    citations = item['bibliographicCitation']
                    if citations is not None:
                        for citation in citations:
                            citation_id = citation

                            if citation_id in dict_documents:
                                citation_doc = dict_documents[citation_id]
                            else:
                                citation_doc = sio.Document(
                                    identifier=citation_id
                                )
                                dict_documents[citation_id] = citation_doc
                            document.bibliographicCitation.append(citation_doc)
                            
                
            # loop through the documents and add them to the graph
            for document in dict_documents.values():

                # create a new RDF resource for the document
                doc_res = onto_ns[document.identifier[0]]

                # add the document properties to the resource
                g.add((doc_res, rdflib.RDF.type, onto_ns['Document']))
                g.add((doc_res, onto_ns['title'], rdflib.Literal(document.title[0])))
                g.add((doc_res, onto_ns['subject'], rdflib.Literal(document.subject[0])))
                g.add((doc_res, onto_ns['created'], rdflib.Literal(document.created[0], datatype=rdflib.XSD.dateTime)))

                # add the authors to the resource
                for author in document.maker:
                    author_res = onto_ns[author.name]
                    g.add((author_res, rdflib.RDF.type, onto_ns['Agent']))
                    g.add((author_res, onto_ns['made'], doc_res))
                    g.add((doc_res, onto_ns['maker'], author_res))

                # add the bibliographic citations to the resource
                for citation_doc in document.bibliographicCitation:
                    citation_res = onto_ns[citation_doc.identifier[0]]
                    g.add((doc_res, onto_ns['bibliographicCitation'], citation_res))

            # save the graph to a file
            g.serialize(destination='onto/my_ontology_sio.owl', format='xml')

    return sio

def owl2turtle(owl_file):
    # Load the RDF/XML file
    graph = rdflib.Graph()
    graph.parse(owl_file, format="xml")

    # Serialize the graph to Turtle format
    turtle_data = graph.serialize(format="turtle")
    turtle_file = owl_file.removesuffix('.owl') + '.ttl'

    # Write the Turtle data to a file
    with open(turtle_file, "wb") as f:
        f.write(turtle_data.encode("utf-8"))

    return turtle_file



'''
# Récupérer la liste des objets dans le bucket
response = s3.list_objects_v2(Bucket=bucket_name_in, Prefix=directory_in)

# Loop à travers les objets dans le bucket
for obj in response['Contents']:
    key = obj['Key']
    # Vérifier si l'objet est un fichier JSON
    if key.endswith('.json'):
    # Télécharger le contenu du fichier JSON depuis S3
        response = s3.get_object(Bucket=bucket_name_in, Key=key)
        json_content = response['Body'].read()
        
        #owl_str = sio.serialize(format="rdfxml").decode("utf-8")
'''        

# -- Création des fichiers .owl et .ttl en local dans le répertoire onto/ --

# Exécuter cette fonction que si vous récupérer le répertoire jsons_with_subject en local
sio = json2owl_local() 

# Convertir le fichier en turtle
owl_file = 'onto/my_ontology_sio.owl'
ttl_file = owl2turtle(owl_file)

# --------- Utilisation AWS (Bucket) --------- #

# Laisser ces lignes en commentaires si vous n'utilisez pas AWS

# Récupération des fichiers contenu dans "jsons_with_subject" depuis un bucket S3
# sio = json2owls3()

# Écrire les données traitées dans un nouveau fichier dans le bucket S3

# 1 - OWL file
# key_owl = directory_out + owl_file.split('/')[-1]
# s3.put_object(Bucket=bucket_name_out, Key=key_owl, Body=owl_file)

# 2 - TURTLE file
# new_key = directory_out + ttl_file.split('/')[-1]
# s3.put_object(Bucket=bucket_name_out, Key=new_key, Body=ttl_file)

print("********* End Creating Ontology and generating files *********")

