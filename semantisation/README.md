# Sémantisation des données contenues dans les publications  d'Arxiv
Ce script permet de télécharger les données contenues dans les articles PDF hébergés sur le site Arxiv.org et de les enregistrer dans une ontologie afin de pouvoir faire des liens entre les documents, les auteurs...

# Prérequis
Python 3.10

## Les packages Python suivants :
- Owlready2
- rdflib

# Utilisation avec docker
## Pré-requis 
- Configurer les accès AWS

## Création et lancement du docker extraction
        docker build -t semantisation .
        docker run -p 8000:8000 semantisation

# Utilisation sans docker
Clonez ce dépôt et naviguez jusqu'au dossier contenant le script.
Exécutez le script semantisation.py à l'aide de la commande python semantisation.py.
Les fichiers .owl et .ttl issues du module seront stockés dans le répertoire onto.

- Pour exécuter le script sur AWS (en utilisant les buckets) : 
    
    - Enlever les commentaires situés devant la fonction json2owls3() et devant les lignes suivants "1 - OWL file" et "2 - TURTLE file"

    - Paramétrer vos variables d'environnement openAI (et AWS si vous souhaitez utiliser les buckets).
        - Sur Linux :

                export aws_access_key_id=xxxxx
                export aws_secret_access_key=xxxxx
                export aws_session_token=xxxxx
                export open_api_key=xxxx

        - Sur Windows :

                $Env:AWS_ACCESS_KEY_ID="xxxx"
                $Env:AWS_SECRET_ACCESS_KEY="xxxx"
                $Env:AWS_SESSION_TOKEN="xxxx"
                $Env:AWS_DEFAULT_REGION="us-east-1"
                $Env:OPENAI_API_KEY="xxxx"


# Fonctionnalités
Le script est divisé en trois fonctions principales : launch_reasoner, json2owl et owl2turtle.

## launch_reasoner
Permet de vérifier la consistance de l'ontologie enrichie à l'aide d'un raisonneur (Hermit). Cela permet de réaliser des inférences et de résoudre des requêtes dans les ontologies OWL.

## json2owl
Cette fonction lit les fichiers JSON présents dans le répertoire jsons_with_subject, convertit les données au format OWL et stocke le tout dans un fichier my_ontology_sio.owl dans le répertoire onto. C'est ce fichier qui sera utilisé par le module suivant (exploitation).

## owl2turtle
Cette fonction lit le fichier OWL présent dans le répertoire onto et le convertit format .ttl et enregistre le fichier my_ontology_sio.ttl dans le répertoire onto.